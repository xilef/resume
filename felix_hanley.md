---
title-meta: Résumé of Felix Hanley
keywords: programmer software engineer devops go golang javascript ruby python php css html openbsd freebsd linux node asn1 ansible postgresql
author: Felix Hanley
papersize: a4
mainfont: DejaVuSerifCondensed
mainfontoptions:
  - Scale=0.99
colorlinks: yes
geometry:
  - margin=1.5cm
---

# [Felix Hanley](http://felixhanley.info)

<felix@userspace.com.au>  
+61 457 092 803  
+61 3 9021 8964  
<https://github.com/felix> | <https://bitbucket.org/xilef>  
_References available on request_

I am an Australian programmer with many years experience in software
engineering, database adminstration and large scale DevOps. I am available for
permanent, contract and remote positions.

With my wife and two young children I have spent the last 10 years doing
part-time volunteer work in SE Asia. I am a native English speaker, am fluent
in Thai and can get by in Lahu.

## [Freestyle Technology](https://freestyleiot.com)

**June 2017 -- Present**

An IoT technology company providing hardware and software solutions to service
providers and municipalities across SE Asia and Australasia. Their solution
involves "smart" devices and real time control.

***Senior Software Engineer***

As a member of the data team I develop ETL and custom rules software to cope
with the constant flow of incoming data and the APIs required to consume this
data by the rest of the system.

- Development of several distributed network stream processing daemons (Go)
- Design and implement numerous based API micro-services (Python, Awk, Shell)
- Design, implement and administer Postgres services and PL/pgSQL functions
- Design and manage big data processing pipelines (InfluxDB)
- Administer several Docker based deployments, both internal and external
- Maintain CI services with custom scripts (Jenkins, Groovy)

**Achievements**

- Development of stream processing software processing 1000s of messages per second
- Good test coverage for all Go, Python & PL/pgSQL code

## [KLData/ARKpX](https://web-beta.archive.org/web/20161203050431/arkpx.com)

**March 2015 -- December 2016**

A small, "Agile" team based in Melbourne developing a cross-platform, encrypted
storage solution much like 'Dropbox' but using client-side, PKI encryption.

***Senior Software Engineer***

I worked remotely from Thailand on AEST's timezone so as to work in sync with
the team and participate in all "daily scrums"

- Lead development of core "vanilla" Javascript encryption library _(Node, Javascript)_
- Implemented PKI primitives using A+ Promises
- Developed custom ASN.1 schemas for CMS (RFC 5952) communications _(ASN.1)_
- Created extensive cross-browser test suite and maintained CI _(Bamboo CI, Selenium)_
- UI development across all modern browsers _(AngularJS)_

**Achievements**

- Multiple product releases to Australian government departments
- Test coverage to 80% across all major browsers
- Important contributions to Open Source components

## [Ayuda Hosting](http://ayudahosting.com.au)

**February 2010 -- February 2015**

Innovative hosting and development company based in Melbourne CBD.

***Senior Software Engineer, DevOps***

As part of the small team I was involved in all areas of network design and
installation, project planning and cost estimation. I worked for one year in
the Melbourne office and then remotely from interstate and overseas.

- Developed client websites and applications _(PHP, Javascript, HTML/CSS)_
- Managed large, redundant networks, firewalls and VMs _(Xen, Keepalived, IPTables, IPSec, PF)_
- Developed control panel and billing software _(PHP, SQL, Shell)_
- Implemented large configuration systems _(CFEngine, Puppet)_
- Monitored and administrated HA and replicated server clusters _(Apache, Nagios, MySQL, Postfix)_

**Achievements**

- Brought many large projects to deployment
- Developed in-house CMS software with custom ORM and plugins
- Improved in-house billing software to simplify account setup and payments

## [User Space](https://userspace.com.au)

**April 2006 -- Present**

Melbourne based hosting and development services to Australian and SE Asian clients.

***Principle programmer, DevOps***

Managed my own hosting and development business.

- Developed custom hosting control panels _(Go, AngularJS, Ruby on Rails, Shell)_
- Maintained 30+ Linux and BSD VMs including HA databases and load balanced servers _(BSD, Ansible, HAProxy, Postgresql)_
- Administered free DNS service, custom dynamic DNS and OpenNIC services _(PowerDNS, TinyDNS, Python)_
- Developed and maintained custom support software for clients _(Python, Shell)_
- Built custom REST API for a number of SaaS clients _(Go, Sinatra)_
- Built custom Magento integrations for international clients _(PHP5, XML)_
- Developed custom Spree (RoRs) plugins and themes for clients _(Ruby, SQL, HTML, CSS)_

**Achievements**

- Built a loyal customer base of over 50 clients

## [Sensory Networks](https://web-beta.archive.org/web/20070105091228/sensorynetworks.com)

**February 2005 -- April 2006**

A provider of software pattern matching and content processing acceleration
solutions with US, London and Sydney offices.

***Web-application developer***

I was part of a two person team that developed and maintained all the internal
software used by the company for tracking stock, sales and hardware testing
results.

- Developed and maintained internal, cross-continent warehouse application _(PHP, HTML, CSS, Lua)_
- Maintained large, replicated database cluster _(MySQL)_
- Performed security and performance audits & penetration testing
- Implemented code documentation framework _(XSLT)_

**Achievements**

- Reduced delays in testing and warehousing by improved UX design and reporting
- Implemented standard documentation schemas for mult-format output

## Freelance

**October 2002 -- February 2005**

***Programmer, DevOps***

I worked in remote mining towns supporting local contractors and government
departments in Western Australia and Sydney.

- Development of custom PHP/MySQL web applications for contractor reporting
- Installed and maintained large LDAP, firewall and email system for local high school
- Install and maintained numerous BSD VPNs and redundant links for local mine contractors

## [National Telecoms Group](https://web-beta.archive.org/web/20080718172405/www.ntgroup.com.au)

**January 2001 -- October 2002**

One of Australia's largest supplier of corporate phone systems, based in Sydney.

***Programmer and DBA***

As a small team of 2 we were responsible for the development and maintenance of
the company's call-centre software and large lead database.

- Developed and maintained a large Saleslogix CRM used by 6+ call centers _(VB, SalesLogix)_
- Administered 300+ seat Citrix Meta-frame installation, the largest in Australia

## DefineIT/Macquarine Health

**2000 -- 2001**

Supplier of medical monitoring equipment, based in Sydney.

***Programmer***

- Maintenance of embedded C/C++ code for monitoring units
- Administration of 50+ seat network and medical records database

## Education

2001 Bachelor of Computer Science and Software Engineering, The University of Sydney

1994 Bachelor of Applied Science and Computer Technology, Swinburne University _(deferred)_
