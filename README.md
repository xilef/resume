# Résumé of Felix Hanley

Nothing fancy.

Source in Markdown with multiple output formats, mainly PDF but also an HTML
fragment for embedding. Requires [Pandoc](http://pandoc.org/) which in turn
requires [TexLive](https://tug.org/texlive/) for PDF output. Generate using
`make` (or `gmake` on BSD).
