# requires gmake

PDOPTS= --smart --latex-engine=xelatex -f markdown+yaml_metadata_block --data-dir=.
DEPENDS= $(wildcard templates/*)

.PHONY: clean

all: pdf html

clean:
	rm -f *.pdf *.html

pdf: felix_hanley.pdf

html: felix_hanley.html

%.pdf: %.md $(DEPENDS)
	pandoc $(PDOPTS) --standalone -o $@ $<

%.html: %.md $(DEPENDS)
	pandoc $(PDOPTS) -o $@ $<

%.docx: %.md $(DEPENDS)
	pandoc $(PDOPTS) -o $@ $<
